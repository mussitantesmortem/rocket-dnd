package main

import (
	"github.com/sirupsen/logrus"
)

var log *logrus.Logger

func main() {
	log = logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	statusManager := newStatusManager(newConfig())
	statusManager.run()
	statusManager.restore()
}
