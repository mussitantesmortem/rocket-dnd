module gitlab.com/mussitantesmortem/rocket-dnd

go 1.16

require (
	github.com/Mephistophiles/Rocket.Chat.Go.SDK v0.0.0-20211215122303-a279f4ca0281
	github.com/hako/durafmt v0.0.0-20210608085754-5c1018a4e16b
	github.com/ktr0731/go-fuzzyfinder v0.6.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.6.1
)
