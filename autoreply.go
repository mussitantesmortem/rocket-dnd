package main

import (
	"fmt"

	"github.com/Mephistophiles/Rocket.Chat.Go.SDK/models"
)

func (s *statusManager) onMsg(msg models.Message) {
	if msg.ID == "" || msg.User.ID == s.userID {
		return
	}

	log.Infof("Got message from %s\n", msg.User.UserName)
	_, answered := s.fuckoffedUsers[msg.User.ID]

	if answered {
		return
	}

	s.fuckoffedUsers[msg.User.ID] = true

	log.Infof("Send autoreply to %s\n", msg.User.UserName)

	textMsg := fmt.Sprintf("%s: (%s)", s.userStatusMessage, getEventDateRange(s.showStatusSince, s.showStatusUntil, false))

	newMsg := s.rocketRealtime.NewMessage(&models.Channel{ID: msg.RoomID}, textMsg)

	_, err := s.rocketRealtime.SendMessage(newMsg)
	if err != nil {
		log.Errorln("Failed to send message", err)

		return
	}

	err = s.rocketRealtime.UnreadMessage(&msg)
	if err != nil {
		log.Errorln("Failed to unread message", err)

		return
	}
}
