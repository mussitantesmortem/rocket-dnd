package main

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"github.com/Mephistophiles/Rocket.Chat.Go.SDK/models"
	"github.com/Mephistophiles/Rocket.Chat.Go.SDK/rest"
	"github.com/hako/durafmt"
)

const (
	hoursInDay      = 24
	messageQueueLen = 10
)

type statusManager struct {
	waitgroup      *sync.WaitGroup
	shutdownSignal chan os.Signal
	fuckoffedUsers map[string]bool
	*config
}

func newStatusManager(config *config) *statusManager {
	shutdownSignal := make(chan os.Signal, 1)
	signal.Notify(shutdownSignal, os.Interrupt)

	return &statusManager{
		new(sync.WaitGroup),
		shutdownSignal,
		make(map[string]bool),
		config,
	}
}

func getLeft(diff time.Duration) string {
	diff = diff.Truncate(time.Minute)
	return durafmt.Parse(diff).String()
}

func getRange(from, to time.Time, forceShowDateRange bool) string {
	const (
		timeFormat   = "15:04"
		shortFormat  = "02.01"
		prettyFormat = "02.01 15:04"
	)

	if to.Sub(from).Hours() > hoursInDay {
		since := from.Format(shortFormat)
		until := to.Format(shortFormat)

		return fmt.Sprintf("%s-%s", since, until)
	}

	if forceShowDateRange {
		since := from.Format(prettyFormat)
		until := to.Format(timeFormat)

		return fmt.Sprintf("%s-%s", since, until)
	}

	until := to.Format(timeFormat)

	return fmt.Sprintf("until %s", until)
}

func getEventDateRange(from, to time.Time, forceShowDateRange bool) string {
	dateRange := getRange(from, to, forceShowDateRange)

	if to.After(time.Now()) {
		dateRange += fmt.Sprintf(", %s left", getLeft(time.Until(to)))
	}

	return dateRange
}

func (s *statusManager) getStatus() *rest.UserStatusResponse {
	status, err := s.rocketRest.GetUserStatus(s.userName)
	if err != nil {
		log.Fatalf("Error get Status: %+v\n", err)
	}

	return status
}

func (s *statusManager) printStatus() {
	currentStatus := s.getStatus()

	log.Infof("My current status is: %s\n", currentStatus.Message)
}

func (s *statusManager) setStatus(status, message string) {
	_, err := s.rocketRest.SetUserStatus(&models.SetUserStatus{
		Status:  status,
		Message: message,
	})
	if err != nil {
		log.Panicf("Error set status: %+v\n", err)
	}

	s.printStatus()
}

func (s *statusManager) enableAutoReply(finishChan <-chan bool) {
	msgChannel := make(chan models.Message, messageQueueLen)

	err := s.rocketRealtime.SubscribeToMyMessages(msgChannel)
	if err != nil {
		log.Panicf("Cannot Subscribe to channel: %+v\n", err)
	}

	s.waitgroup.Add(1)

	go func() {
		log.Infoln("Listen for events...")

		for {
			select {
			case msg := <-msgChannel:
				s.onMsg(msg)
			case <-finishChan:
				s.waitgroup.Done()

				return
			}
		}
	}()
}

func (s *statusManager) listenEvents() {
	finishChan := make(chan bool)
	firstWait := true

	s.waitgroup.Add(1)

outer:
	for !time.Now().After(s.showStatusUntil) {
		msg := getEventDateRange(s.showStatusSince, s.showStatusUntil, false)
		busyMessage := fmt.Sprintf("%s: %s", s.userStatusMessage, msg)
		s.setStatus(s.userStatus, busyMessage)

		if s.needAutoreply {
			if firstWait {
				firstWait = false

				s.enableAutoReply(finishChan)
			}
		}

		neededSleep := time.Until(s.showStatusUntil) % (s.updateInterval)

		if neededSleep == 0 {
			neededSleep = s.updateInterval
		}

		neededSleep = neededSleep - 10*time.Millisecond

		log.Debugf("Sleep by %+v\n", neededSleep)

		select {
		case <-s.shutdownSignal:
			break outer
		case <-time.After(neededSleep):
			continue
		}
	}

	if !firstWait && s.needAutoreply {
		finishChan <- true
	}

	s.waitgroup.Done()
}

func (s *statusManager) run() {
	if len(s.userStatusMessage) > 0 {
		s.listenEvents()
	}

	s.waitgroup.Wait()
}

func (s *statusManager) restore() {
	msg := getEventDateRange(s.showStatusSince, s.showStatusUntil, true)
	restoreMsg := fmt.Sprintf("Was %s: %s", strings.ToLower(s.userStatusMessage), msg)
	s.setStatus(s.restoreUserStatus, restoreMsg)
}
