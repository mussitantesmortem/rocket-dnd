package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"net/url"
	"os"
	"path"
	"strings"
	"time"

	"github.com/Mephistophiles/Rocket.Chat.Go.SDK/models"
	"github.com/Mephistophiles/Rocket.Chat.Go.SDK/realtime"
	"github.com/Mephistophiles/Rocket.Chat.Go.SDK/rest"
	"github.com/ktr0731/go-fuzzyfinder/matching"
)

type mode int

var errInvalidDate = errors.New("invalid date")

const (
	customDND mode = iota + 1
	commuting
	inOffice
	meeting
	outSick
	vacationing
	workingRemotely
)

const (
	userStatusAway   = "away"
	userStatusOnline = "online"
	userStatusBusy   = "busy"
)

type userConfig struct {
	RocketChatServer   string `json:"rocketchat_server"`
	RocketChatEmail    string `json:"rocketchat_email"`
	RocketChatPassword string `json:"rocketchat_password"`
}

type config struct {
	userID   string
	userName string

	rocketRest     *rest.Client
	rocketRealtime *realtime.Client

	userStatus        string
	restoreUserStatus string
	userStatusMessage string

	needAutoreply bool
	mode          mode

	showStatusSince time.Time
	showStatusUntil time.Time

	updateInterval time.Duration
}

func dateRound(parsedDate time.Time) time.Time {
	now := time.Now()

	return time.Date(
		now.Year(),
		parsedDate.Month(),
		parsedDate.Day(),
		0,
		0,
		0,
		0,
		now.Location(),
	)
}

func timeRound(parsedDate time.Time) time.Time {
	now := time.Now()

	return time.Date(
		now.Year(),
		now.Month(),
		now.Day(),
		parsedDate.Hour(),
		parsedDate.Minute(),
		0,
		0,
		now.Location(),
	)
}

func tryToParseDate(date string) (time.Time, error) {
	possibleFormats := []string{
		"2006-01-02 15:04:05",
		"2006-01-02 15:04",
		"2006-01-02 15",
		"2006-01-02",
		"02-01",
		"02.01",
		"15:04:05",
		"15:04",
	}

	for _, format := range possibleFormats {
		date, err := time.Parse(format, date)
		if err != nil {
			continue
		}

		switch format {
		case "02-01":
		case "02.01":
			date = dateRound(date)
		case "15:04:05":
		case "15:04":
			date = timeRound(date)
		}

		// set current timezone
		date = time.Date(
			date.Year(),
			date.Month(),
			date.Day(),
			date.Hour(),
			date.Minute(),
			date.Second(),
			date.Nanosecond(),
			time.Now().Location(),
		)

		return date, nil
	}

	return time.Now(), errInvalidDate
}

func parseDate(from time.Time, date string) time.Time {
	const (
		OnlyHours        = 1
		HoursWithMinutes = 2
	)

	parsedDate, err := tryToParseDate(date)

	if err == nil {
		return parsedDate
	}

	var diffHours, diffMinutes int64

	n, _ := fmt.Sscanf(date, "+%d:%d", &diffHours, &diffMinutes)

	switch n {
	case OnlyHours:
		return from.Add(time.Hour * time.Duration(diffHours))
	case HoursWithMinutes:
		return from.Add(time.Hour*time.Duration(diffHours) + time.Minute*time.Duration(diffMinutes))
	default:
		log.Fatalf("Parse date failed '%s'\n", date)
	}

	return time.Time{}
}

func loadConfig() *config {
	dir, err := os.UserConfigDir()
	if err != nil {
		log.Fatalf("Cannot load config dir '%+v'\n", err)
	}

	fileName := path.Join(dir, "rocket-dnd", "config.json")

	file, err := os.ReadFile(fileName)
	if err != nil {
		log.Fatalf("Cannot read file %s: %+v", fileName, err)
	}

	cfg := new(userConfig)

	err = json.Unmarshal(file, &cfg)
	if err != nil {
		log.Fatalf("Cannot parse file %s: %+v", fileName, err)
	}

	if cfg.RocketChatServer == "" {
		log.Fatalln("RocketChat server is empty!")
	}

	if cfg.RocketChatEmail == "" {
		log.Fatalln("RocketChat email is empty!")
	}

	if cfg.RocketChatPassword == "" {
		log.Fatalln("RocketChat password is empty!")
	}

	url, err := url.Parse(cfg.RocketChatServer)
	if err != nil {
		log.Fatalf("Failed to parse url '%s': %+v\n", url, err)
	}

	currentUserName := strings.Split(cfg.RocketChatEmail, "@")[0]

	rtClient, err := realtime.NewClient(url, false)
	if err != nil {
		log.Panicf("Error create client: %+v\n", err)
	}

	// login as the main admin user
	login := models.UserCredentials{
		Email:    cfg.RocketChatEmail,
		Password: cfg.RocketChatPassword,
	}

	realtimeLoginResult, err := rtClient.Login(&login)
	if err != nil {
		log.Panicf("Error realtime login: %+v\n", err)
	}

	restClient := rest.NewClient(url, false)
	err = restClient.Login(&models.UserCredentials{
		Token: realtimeLoginResult.Token,
		ID:    realtimeLoginResult.ID,
	})

	if err != nil {
		log.Panicf("Error realtime login: %+v\n", err)
	}

	return &config{
		userID:   realtimeLoginResult.ID,
		userName: currentUserName,

		rocketRest:     restClient,
		rocketRealtime: rtClient,

		userStatus:        "online",
		restoreUserStatus: "busy",
		userStatusMessage: "",

		needAutoreply: false,
		mode:          customDND,

		showStatusSince: time.Time{},
		showStatusUntil: time.Time{},
	}
}

func parseMode(modeToParse string) mode {
	availableModes := map[string]mode{
		"dnd":         customDND,
		"meeting":     meeting,
		"inoffice":    inOffice,
		"remotely":    workingRemotely,
		"sick":        outSick,
		"vacationing": vacationing,
		"commuting":   commuting,
	}

	availableModesArr := []string{}

	for k := range availableModes {
		availableModesArr = append(availableModesArr, k)
	}

	matched := matching.FindAll(modeToParse, availableModesArr)

	if len(matched) != 1 {
		log.Fatalf("Invalid mode '%s'\n", modeToParse)
	}

	matchedMode := availableModesArr[matched[0].Idx]

	return availableModes[matchedMode]
}

func newConfig() *config {
	var possibleDateFrom, possibleDateTo, statusMessage, atExitStatus, mode string
	var updateInterval int

	var autoreply bool

	now := time.Now().Format("2006-01-02 15:04")

	flag.BoolVar(&autoreply, "autoreply", false, "autoreply on incoming messages")
	flag.IntVar(&updateInterval, "update-interval", 5, "update interval in minutes")
	flag.StringVar(&statusMessage, "status", "", "specify status")
	flag.StringVar(&atExitStatus, "atexit-status", "", "specify status to set at exit")
	flag.StringVar(&possibleDateFrom, "since", now, "since this date show status")
	flag.StringVar(&possibleDateTo, "until", now, "until this date show status")
	flag.StringVar(&mode, "mode", "dnd", "work mode")
	flag.Parse()

	sinceDate := parseDate(time.Now(), possibleDateFrom)
	untilDate := parseDate(sinceDate, possibleDateTo)

	if untilDate.Hour() == 0 && untilDate.Minute() == 0 && untilDate.Second() == 0 {
		untilDate = time.Date(
			untilDate.Year(),
			untilDate.Month(),
			untilDate.Day(),
			23,
			59,
			59,
			0,
			untilDate.Location(),
		)
	}

	cfg := loadConfig()

	cfg.needAutoreply = autoreply

	switch parseMode(mode) {
	case customDND:
		cfg.userStatus = userStatusAway
		cfg.restoreUserStatus = userStatusOnline
		cfg.mode = customDND
		cfg.userStatusMessage = statusMessage
	case meeting:
		cfg.userStatus = userStatusBusy
		cfg.restoreUserStatus = userStatusOnline
		cfg.mode = meeting
		cfg.userStatusMessage = "🗒️ In a meeting"
	case inOffice:
		cfg.userStatus = userStatusOnline
		cfg.restoreUserStatus = userStatusAway
		cfg.mode = inOffice
		cfg.userStatusMessage = "🏢 In the office"
	case workingRemotely:
		cfg.userStatus = userStatusOnline
		cfg.restoreUserStatus = userStatusAway
		cfg.mode = workingRemotely
		cfg.userStatusMessage = "🏡 Working remotely"
	case outSick:
		cfg.userStatus = userStatusAway
		cfg.restoreUserStatus = userStatusOnline
		cfg.mode = outSick
		cfg.userStatusMessage = "🤒 Out sick"
	case vacationing:
		cfg.userStatus = userStatusBusy
		cfg.restoreUserStatus = userStatusOnline
		cfg.mode = vacationing
		cfg.userStatusMessage = "🌴 Vacationing"
	case commuting:
		cfg.userStatus = userStatusAway
		cfg.restoreUserStatus = userStatusOnline
		cfg.mode = commuting
		cfg.userStatusMessage = "🚌 Commuting"
	}

	cfg.showStatusSince = sinceDate
	cfg.showStatusUntil = untilDate

	cfg.updateInterval = time.Duration(updateInterval) * time.Minute

	return cfg
}
