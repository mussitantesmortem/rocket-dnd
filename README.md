# rocket-dnd

## Getting started

Install via:

```shell
go install gitlab.com/mussitantesmortem/rocket-dnd@latest
```

## Usage

```
Usage of rocket-dnd:
  -atexit-status string
    	specify status to set at exit
  -autoreply
    	autoreply on incoming messages
  -mode string
    	work mode (default "dnd")
  -since string
    	since this date show status (default "2022-06-29 18:24")
  -status string
    	specify status
  -until string
    	until this date show status (default "2022-06-29 18:24")
  -update-interval int
    	update interval in minutes (default 5)
```
