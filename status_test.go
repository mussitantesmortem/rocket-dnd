package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetLeft(t *testing.T) {
	assert.Equal(t, getLeft(time.Hour*0), "0 seconds")
	assert.Equal(t, getLeft(time.Hour*1), "1 hour")
	assert.Equal(t, getLeft(time.Hour*24), "1 day")
	assert.Equal(t, getLeft(time.Hour*1+time.Minute*5), "1 hour 5 minutes")
	assert.Equal(t, getLeft(time.Hour*1+time.Minute*59), "1 hour 59 minutes")
	assert.Equal(t, getLeft(time.Hour*14+time.Minute*59), "14 hours 59 minutes")
}

func TestGetRange(t *testing.T) {
	from := time.Date(2000, 01, 01, 12, 00, 00, 00, time.UTC)
	to := time.Date(2000, 01, 01, 18, 00, 00, 00, time.UTC)

	assert.Equal(t, getRange(from, to, false), "until 18:00")
	assert.Equal(t, getRange(from, to, true), "01.01 12:00-18:00")

	to = time.Date(2000, 02, 01, 18, 00, 00, 00, time.UTC)

	assert.Equal(t, getRange(from, to, false), "01.01-01.02")
	assert.Equal(t, getRange(from, to, true), "01.01-01.02")
}
